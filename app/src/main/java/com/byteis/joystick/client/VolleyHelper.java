package com.byteis.joystick.client;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.byteis.joystick.custom.CustomJSONObjectRequest;
import com.byteis.joystick.model.ServerResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by Dina on 21/11/2015.
 */
public class VolleyHelper {

    protected Context context;
    protected RequestQueue mRequestQueue;

    protected VolleyHelper(Context context) {
        this.context = context;
        mRequestQueue = Volley.newRequestQueue(context);
    }

    protected void scheduleJsonObjectRequest(int request, String requestUrl, JSONObject jsonObject, final ClientCallback clientCallback, final Class<?> classType) {
        CustomJSONObjectRequest jsonObjReq = new CustomJSONObjectRequest(request,
                requestUrl, jsonObject,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Object object = null;

                        if (classType != null) {
                            Gson gson = new Gson();
                            object = gson.fromJson(response.toString(), classType);
                        }
                        clientCallback.onSuccess(object);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                ServerResponse serverResponse = null;
                Gson gson = new Gson();
                String json = null;

                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    json = new String(response.data);
                    serverResponse = gson.fromJson(json, ServerResponse.class);
                }
                clientCallback.onFailure(serverResponse);
            }
        }, context);

        jsonObjReq.setRetryPolicy(new

                        DefaultRetryPolicy(75000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );
        mRequestQueue.add(jsonObjReq);
    }

}
