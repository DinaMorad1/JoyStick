package com.byteis.joystick.client;

import android.content.Context;

import com.android.volley.Request;
import com.byteis.joystick.R;
import com.byteis.joystick.adapter.UserAdapter;
import com.byteis.joystick.model.ServerResponse;
import com.byteis.joystick.model.User;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Dina on 21/11/2015.
 */
public class ApiClientImp extends VolleyHelper implements ApiClient {

    private static ApiClientImp instance;
    private String baseUrl;

    public static ApiClientImp getInstance(Context context) {
        if (instance == null)
            instance = new ApiClientImp(context);
        return instance;
    }

    private ApiClientImp(Context context) {
        super(context);
        baseUrl = context.getResources().getString(R.string.base_url);
    }


    @Override
    public void userSignup(User user, ClientCallback callback) {
        //excludeFieldsWithoutExposeAnnotation
        Gson gson = new GsonBuilder().registerTypeAdapter(User.class, new UserAdapter()).create();
        String jsonStr = gson.toJson(user);
        JSONObject userJsonObj = new JSONObject();
        JSONObject paramsJsonObj = new JSONObject();
        try {
            userJsonObj = new JSONObject(jsonStr);
            paramsJsonObj.put(ApiClient.USER, userJsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        scheduleJsonObjectRequest(Request.Method.POST, baseUrl + ApiClient.USER_URL, paramsJsonObj, callback, User.class);
    }

    @Override
    public void forgotPassword(String email, ClientCallback callback) {
        scheduleJsonObjectRequest(Request.Method.GET, baseUrl + ApiClient.USER_FORGOT_PASSWORD_URL + "?" + ApiClient.USER_EMAIL + "=" + email, null, callback, ServerResponse.class);
    }

    @Override
    public void userLogin(User user, ClientCallback callback) {
        Gson gson = new GsonBuilder().registerTypeAdapter(User.class, new UserAdapter()).create();
        String jsonStr = gson.toJson(user);
        JSONObject userJsonObj = new JSONObject();
        JSONObject paramsJsonObj = new JSONObject();
        try {
            userJsonObj = new JSONObject(jsonStr);
            paramsJsonObj.put(ApiClient.USER, userJsonObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        scheduleJsonObjectRequest(Request.Method.POST, baseUrl + ApiClient.USER_SIGNIN_URL, paramsJsonObj, callback, User.class);
    }
}
