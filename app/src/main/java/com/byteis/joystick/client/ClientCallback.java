package com.byteis.joystick.client;

import com.byteis.joystick.model.ServerResponse;

/**
 * Created by Dina on 10/11/2015.
 */
public interface ClientCallback<T> {

    public void onSuccess(T t);

    public void onFailure(ServerResponse serverResponse);

}
