package com.byteis.joystick.client;

import com.byteis.joystick.model.User;

/**
 * Created by Dina on 21/11/2015.
 */
public interface ApiClient {

    /*requests urls*/
    public static final String USER_URL = "/users";
    public static final String USER_FORGOT_PASSWORD_URL = USER_URL + "/password/new";
    public static final String USER_SIGNIN_URL = USER_URL + "/sign_in";

    /*user fields*/
    public static final String USER = "user";
    public static final String USER_EMAIL = "email";
    public static final String USER_PASSWORD = "password";
    public static final String USER_NAME = "name";

    public void userLogin(User user, ClientCallback callback);

    public void userSignup(User user, ClientCallback callback);

    public void forgotPassword(String email, ClientCallback callback);

}
