package com.byteis.joystick.model;

import com.google.gson.internal.LinkedTreeMap;

/**
 * Created by Dina on 21/11/2015.
 */
public class ServerResponse {

    private boolean success;

    private String message;

    private LinkedTreeMap errors;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public LinkedTreeMap getErrors() {
        return errors;
    }

    public void setErrors(LinkedTreeMap errors) {
        this.errors = errors;
    }
}
