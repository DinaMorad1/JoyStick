package com.byteis.joystick.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.byteis.joystick.client.ApiClient;
import com.byteis.joystick.util.UtilityMethods;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Dina on 21/11/2015.
 */
@Table(name = "User")
public class User extends Model {

    @Column(name = "email")
    @SerializedName(ApiClient.USER_EMAIL)
    private String email;

    @Column(name = "password")
    @SerializedName(ApiClient.USER_PASSWORD)
    private String password;

    @Column(name = "UserName")
    @SerializedName(ApiClient.USER_NAME)
    private String userName;

    @Column(name = "ServerId")
    @SerializedName("id")
    private String serverId;

    @SerializedName("auth_token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public boolean isValidEmail() {
        if (email == null || "".equals(email.trim()) || !UtilityMethods.isEmailValid(email))
            return false;
        return true;
    }

    public boolean isValidPassword(){
        if (password == null || "".equals(password.trim()))
            return false;
        return true;
    }

    public boolean isValidUsername(){
        if (userName == null || "".equals(userName.trim()))
            return false;
        return true;
    }
}
