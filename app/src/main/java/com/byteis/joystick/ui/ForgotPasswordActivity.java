package com.byteis.joystick.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import com.byteis.joystick.R;
import com.byteis.joystick.client.ClientCallback;
import com.byteis.joystick.databinding.ActivityForgotPasswordBinding;
import com.byteis.joystick.model.ServerResponse;
import com.byteis.joystick.util.UtilityMethods;

/**
 * Created by Dina on 22/11/2015.
 */
public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener, ClientCallback<ServerResponse> {

    private ActivityForgotPasswordBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        binding.setListener(this);
        setToolBarContent(binding.toolbar, getString(R.string.forgot_password_title), true, true);
    }


    @Override
    public void onClick(View v) {
        resetValidationErrors();
        if (isValidModel()) {
            showProgressDialog(getString(R.string.please_Wait), false, true);
            String email = binding.emailEditText.getText().toString();
            apiClient.forgotPassword(email, this);
        }
    }

    private void resetValidationErrors() {
        setError(binding.emailTextInput, null);
    }

    private boolean isValidModel() {
        boolean isValid = true;
        String email = binding.emailEditText.getText().toString();
        if (email == null || "".equals(email.trim()) || !UtilityMethods.isEmailValid(email)) {
            isValid = false;
            UtilityMethods.showSnackbar(getString(R.string.invalid_fields), binding.layoutForgotPassword, this);
            setError(binding.emailTextInput, getString(R.string.invalid_email));
        }
        return isValid;
    }

    @Override
    public void onSuccess(ServerResponse serverResponse) {
        if (serverResponse != null && serverResponse.getMessage() != null && !"".equals(serverResponse.getMessage().trim()))
            UtilityMethods.showSnackbar(serverResponse.getMessage(), binding.layoutForgotPassword, this);
        dismissProgressDialog();
    }

    @Override
    public void onFailure(ServerResponse serverResponse) {

        dismissProgressDialog();
    }
}
