package com.byteis.joystick.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.byteis.joystick.R;
import com.byteis.joystick.client.ApiClient;
import com.byteis.joystick.client.ClientCallback;
import com.byteis.joystick.databinding.ActivityLoginBinding;
import com.byteis.joystick.model.ServerResponse;
import com.byteis.joystick.model.User;
import com.byteis.joystick.util.UtilityMethods;

import java.util.ArrayList;

/**
 * Created by Dina on 20/11/2015.
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener, TextWatcher, ClientCallback<User> {

    private ActivityLoginBinding binding;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        binding.setListener(this);
        user = new User();
        binding.setUser(user);
        binding.setWatcher(this);
    }

    public void onRegisterClicked(View view) {
        Intent signupIntent = new Intent(this, SignupActivity.class);
        startActivityWithTransition(this, signupIntent);
    }

    public void onForgotPasswordClicked(View view) {
        Intent forgotPasswordIntent = new Intent(this, ForgotPasswordActivity.class);
        startActivityWithTransition(this, forgotPasswordIntent);
    }

    private void resetValidatinErrors(){
        setError(binding.emailTextInput, null);
        setError(binding.passwordTextInput, null);
    }

    @Override
    public void onClick(View v) {
        resetValidatinErrors();
        if (isValidData()){
            showProgressDialog(getString(R.string.please_Wait), false, true);
            apiClient.userLogin(user, this);
        }
    }

    private boolean isValidData() {
        boolean isValid = true;

        if (!user.isValidEmail()) {
            setError(binding.emailTextInput, getString(R.string.invalid_email));
            isValid = false;
        }
        if (!user.isValidPassword()) {
            setError(binding.passwordTextInput, getString(R.string.invalid_password));
            isValid = false;
        }
        if (!isValid)
            UtilityMethods.showSnackbar(getString(R.string.invalid_fields), binding.layoutLogin, this);
        return isValid;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (editable == binding.emailEditText.getEditableText())
            user.setEmail(binding.emailEditText.getText().toString());
        else if (editable == binding.passwordEditText.getEditableText())
            user.setPassword(binding.passwordEditText.getText().toString());
    }

    @Override
    public void onSuccess(User responseUser) {
        if (responseUser != null) {
            if (responseUser.getEmail() == null || "".equals(responseUser.getEmail().trim()))
                responseUser.setEmail(binding.emailEditText.getText().toString());
            responseUser.save();
        }

        UtilityMethods.showSnackbar(getString(R.string.login_success), binding.layoutLogin, this);
        dismissProgressDialog();
    }

    @Override
    public void onFailure(ServerResponse serverResponse) {
        String errorMessage = getString(R.string.server_error);
        if (serverResponse != null && serverResponse.getMessage() != null && !"".equals(serverResponse.getMessage().trim()))
            errorMessage = serverResponse.getMessage();

        UtilityMethods.showSnackbar(errorMessage, binding.layoutLogin, this);

        if (serverResponse.getErrors() != null && serverResponse.getErrors().size() > 0) {
            if (serverResponse.getErrors().containsKey(ApiClient.USER_EMAIL))
                setError(binding.emailTextInput, (String) ((ArrayList) serverResponse.getErrors().get(ApiClient.USER_EMAIL)).get(0));
            if (serverResponse.getErrors().containsKey(ApiClient.USER_PASSWORD))
                setError(binding.passwordTextInput, (String) ((ArrayList) serverResponse.getErrors().get(ApiClient.USER_PASSWORD)).get(0));
        }
        dismissProgressDialog();
    }
}
