package com.byteis.joystick.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.transition.Slide;
import android.transition.Transition;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;

import com.byteis.joystick.R;
import com.byteis.joystick.client.ApiClient;
import com.byteis.joystick.client.ApiClientImp;

/**
 * Created by Dina on 20/11/2015.
 */
public class BaseActivity extends AppCompatActivity {

    protected ApiClient apiClient;
    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiClient = ApiClientImp.getInstance(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.primary_color_dark));
        }

        applyTransition(this);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected Activity applyTransition(Activity activity) {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            Transition transition = new Slide();
            ((Slide) transition).setSlideEdge(Gravity.RIGHT);
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            transition.excludeTarget(android.R.id.navigationBarBackground, true);
            activity.getWindow().setEnterTransition(transition);
        }
        return activity;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startActivityWithTransition(Activity activity, Intent intent) {
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, null);
        applyTransition(activity);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    protected void finishActivity(Activity activity) {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= Build.VERSION_CODES.LOLLIPOP) {
            activity.finishAfterTransition();
        } else {
            activity.finish();
        }
    }

    protected void setError(TextInputLayout textInputLayout, String error) {
        textInputLayout.setError(error);
        textInputLayout.getEditText().setError(error);
    }

    protected void showProgressDialog(String dialogMessage, boolean isCancelable, boolean isIndeteminate) {
       dismissProgressDialog();
        if (progressDialog == null)
            progressDialog = new ProgressDialog(this);

        progressDialog.setMessage(dialogMessage);
        progressDialog.setCancelable(isCancelable);
        progressDialog.setIndeterminate(isIndeteminate);
        progressDialog.show();
    }

    protected void dismissProgressDialog(){
        if (progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }

    protected void setToolBarContent(Toolbar toolbar, String title, boolean showTitle, boolean showHome) {
        setSupportActionBar(toolbar);
        if (!"".equals(title)) {
            getSupportActionBar().setTitle(title);
        }
        getSupportActionBar().setDisplayShowTitleEnabled(showTitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(showHome);
    }
}
