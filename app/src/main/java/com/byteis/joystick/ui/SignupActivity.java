package com.byteis.joystick.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.byteis.joystick.R;
import com.byteis.joystick.client.ApiClient;
import com.byteis.joystick.client.ClientCallback;
import com.byteis.joystick.databinding.ActivitySignupBinding;
import com.byteis.joystick.model.ServerResponse;
import com.byteis.joystick.model.User;
import com.byteis.joystick.util.UtilityMethods;

import java.util.ArrayList;

/**
 * Created by Dina on 21/11/2015.
 */
public class SignupActivity extends BaseActivity implements View.OnClickListener, ClientCallback<User>, TextWatcher {

    private ActivitySignupBinding binding;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        binding.setListener(this);
        user = new User();
        binding.setUser(user);
        binding.setWatcher(this);
    }

    @Override
    public void onClick(View v) {
        clearValidationErrors();
        if (isValidData()) {
            showProgressDialog(getString(R.string.please_Wait), false, true);
            apiClient.userSignup(user, this);
        }
    }

    private boolean isValidData() {
        boolean isValid = true;

        if (!user.isValidEmail()) {
            setError(binding.emailTextInput, getString(R.string.invalid_email));
            isValid = false;
        }
        if (!user.isValidPassword()) {
            setError(binding.passwordTextInput, getString(R.string.invalid_password));
            isValid = false;
        }
        if (!user.isValidUsername()) {
            setError(binding.usernameTextInput, getString(R.string.invalid_username));
            isValid = false;
        }
        if (!isValid)
            UtilityMethods.showSnackbar(getString(R.string.invalid_fields), binding.layoutSignup, this);
        return isValid;
    }

    @Override
    public void onSuccess(User responseUser) {
        if (responseUser != null) {
            if (responseUser.getEmail() == null || "".equals(responseUser.getEmail().trim()))
                responseUser.setEmail(binding.emailEditText.getText().toString());
            responseUser.save();
        }

        UtilityMethods.showSnackbar(getString(R.string.user_successfully_created), binding.layoutSignup, this);
        dismissProgressDialog();
    }

    @Override
    public void onFailure(ServerResponse serverResponse) {
        String errorMessage = getString(R.string.server_error);
        if (serverResponse != null && serverResponse.getMessage() != null && !"".equals(serverResponse.getMessage().trim()))
            errorMessage = serverResponse.getMessage();

        UtilityMethods.showSnackbar(errorMessage, binding.layoutSignup, this);

        if (serverResponse.getErrors() != null && serverResponse.getErrors().size() > 0) {
            if (serverResponse.getErrors().containsKey(ApiClient.USER_EMAIL))
                setError(binding.emailTextInput, (String) ((ArrayList) serverResponse.getErrors().get(ApiClient.USER_EMAIL)).get(0));
            if (serverResponse.getErrors().containsKey(ApiClient.USER_PASSWORD))
                setError(binding.passwordTextInput, (String) ((ArrayList) serverResponse.getErrors().get(ApiClient.USER_PASSWORD)).get(0));
            if (serverResponse.getErrors().containsKey(ApiClient.USER_NAME))
                setError(binding.usernameTextInput, (String) ((ArrayList) serverResponse.getErrors().get(ApiClient.USER_NAME)).get(0));
        }
        dismissProgressDialog();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    //ToDo: refactor using model 2 way data binding
    @Override
    public void afterTextChanged(Editable editable) {
        if (editable == binding.emailEditText.getEditableText())
            user.setEmail(binding.emailEditText.getText().toString());
        else if (editable == binding.passwordEditText.getEditableText())
            user.setPassword(binding.passwordEditText.getText().toString());
        else if (editable == binding.usernameEditText.getEditableText())
            user.setUserName(binding.usernameEditText.getText().toString());
    }

    private void clearValidationErrors() {
        setError(binding.emailTextInput, null);
        setError(binding.passwordTextInput, null);
        setError(binding.usernameTextInput, null);
    }
}
