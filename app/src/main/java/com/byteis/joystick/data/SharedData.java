package com.byteis.joystick.data;

import android.app.Application;

import com.activeandroid.ActiveAndroid;

/**
 * Created by Dina on 21/11/2015.
 */
public class SharedData extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);

    }
}
